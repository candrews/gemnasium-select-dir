# multiple-java

Example project for https://gitlab.com/gitlab-org/gitlab/-/issues/465799

This project contains 2 simple Java projects: one using maven and one using Gradle.

Gemnasium is invoked using this command: `/analyzer sbom --target-dir maven --artifact-dir maven`

Expected: Gemnasium should only look at the `maven` directory. There should be an SBOM created at `maven/gl-*.cdx.json`.

Actual: Gemnasium appears to ignore the `--target-dir` and `artifact-dir` arguments. It scans the `gradle` directory and produces an SBOM within it.

See the pipeline for this project.
